package com.vfd.mfb.savi.user.utils;

import com.sendgrid.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.Map;

@Slf4j
@Component
public class EmailHelper {
    @Value("${sendgrid.key}")
    private String sendGridKey;

    @Value("${savi.config.email}")
    private String emailFrom;

    @Autowired
    private TemplateEngine templateEngine;

    public void sendEmail(String subject, String emailTo, String emailTemlate, Map emailData) throws IOException {
        Email from = new Email(emailFrom);
        Email to = new Email(emailTo);

        Context context = new Context();
        context.setVariables(emailData);
        String html = templateEngine.process(emailTemlate, context);
        Content content = new Content("text/html", html);
        
        Mail mail = new Mail(from, subject, to, content);

        System.out.println("from "+from);
        System.out.println("to "+to);
       
        try {
            SendGrid sg = new SendGrid(sendGridKey);
            Request request = new Request();
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mail.build());

            Response response = sg.api(request);
            // log.info(response.getStatusCode());
            // log.info(response.getBody());
            // log.info(response.getHeaders());
        } catch (IOException ex) {
            throw ex;
        }
    }
}