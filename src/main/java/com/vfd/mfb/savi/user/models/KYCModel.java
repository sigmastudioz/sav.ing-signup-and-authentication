package com.vfd.mfb.savi.user.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.vfd.mfb.savi.user.utils.ShortHash;

import org.hibernate.annotations.NaturalId;
import org.hibernate.annotations.Type;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;
import java.util.Date;


@JsonIgnoreProperties(
    value = {"id", "createdAt", "updatedAt"},
    allowSetters = true
)
@Data
@NoArgsConstructor
@Entity
@Table(name = "kyc_documents", uniqueConstraints = {
    @UniqueConstraint(columnNames = {
        "userId"
    })
})
public class KYCModel extends AuditModel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long Id;

    @NotNull
    @Column(columnDefinition = "VARCHAR(50)", nullable = false)
    private String userId;

    @Column(columnDefinition = "NUMERIC(10) default null")
    private Long BVN;

    @Column(columnDefinition = "BOOLEAN default false")
    private boolean verifiedBVN;

    @Column(columnDefinition = "VARCHAR(350) default null")
    private String profileImage;

    @Column(columnDefinition = "BOOLEAN default false")
    private boolean verifiedProfileImage;

    @Column(columnDefinition = "NUMERIC(15) default null")
    private Long depositBank;

    @Column(columnDefinition = "NUMERIC(8) default null")
    private Long depositBankCode;

    @Column(columnDefinition = "BOOLEAN default false")
    private boolean verifiedDepositBank;

    @Column(columnDefinition = "VARCHAR(350) default null")
    private String idImage;

    @Column(columnDefinition = "BOOLEAN default false")
    private boolean verifiedID;
}

