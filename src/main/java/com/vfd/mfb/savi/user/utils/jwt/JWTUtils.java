package com.vfd.mfb.savi.user.utils.jwt;

import java.util.Date;

import com.vfd.mfb.savi.user.exception.GenericException;
import com.vfd.mfb.savi.user.models.UserModel;
import com.vfd.mfb.savi.user.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data
@Component
public class JWTUtils {
    @Value("${security.jwt.publicKey}")
    private String publicKey;

    @Value("${security.jwt.uri:/register/**}")
    private String Uri;

    @Value("${security.jwt.header:Authorization}")
    private String header;

    @Value("${security.jwt.prefix:Bearer }")
    private String prefix;

    @Value("${security.jwt.secret:JwtSecretKey}")
    private String secret;

    @Value("${security.jwt.issuer}")
    private String issuer;

    @Autowired
    private UserRepository userRepository;

    public String createJWT(UserModel user, int expiration) {
  
        //The JWT signature algorithm we will be using to sign the token
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
    
        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);
    
        Claims claims = Jwts.claims().setSubject(user.getUserId());
        claims.put("firstName", user.getFirstName() + "");
        claims.put("lastName", user.getLastName() + "");
        // claims.put("role", user.getRole());

        //Let's set the JWT Claims
        JwtBuilder builder = Jwts.builder()
            .setIssuedAt(now)
            .setIssuer(issuer)
            .setClaims(claims)
            .signWith(signatureAlgorithm, secret);
      
        //if it has been specified, let's add the expiration
        if (expiration > 0) {
            long expMillis = nowMillis + expiration;
            Date exp = new Date(expMillis);
            builder.setExpiration(exp);
        }  
      
        //Builds the JWT and serializes it to a compact, URL-safe string
        return builder.compact();
    }

    public UserModel decodeJWT(String jwt) throws GenericException {
        UserModel user = new UserModel();
        try {
            if (jwt == null) {
                user.setUserId(null);
            } else {
                //This line will throw an exception if it is not a signed JWS (as expected)
                Claims claims = Jwts.parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(getJWTToken(jwt))
                    .getBody();

                user.setUserId(claims.getSubject());
                user.setFirstName(claims.get("firstName").toString());
                user.setLastName(claims.get("lastName").toString());

                log.info("Valided subject: {}", claims.getSubject());
            }
        } catch (JwtException | IllegalArgumentException e) {
            throw new GenericException("Expired or invalid JWT token");
        }

        return user;
    }

    public String getJWTToken(String bearerToken) {
        if (bearerToken != null && bearerToken.startsWith(prefix)) {
            return bearerToken.substring(7);
        }
        return null;
    }
}