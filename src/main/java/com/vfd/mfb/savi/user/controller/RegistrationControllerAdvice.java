package com.vfd.mfb.savi.user.controller;

import java.sql.SQLException;

import com.vfd.mfb.savi.user.exception.GenericException;
import com.vfd.mfb.savi.user.exception.InternalServerException;
import com.vfd.mfb.savi.user.exception.SQLErrorException;
import com.vfd.mfb.savi.user.models.reponse.implemenation.GenericResponse;
import com.vfd.mfb.savi.user.models.reponse.implemenation.UserResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingRequestHeaderException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.MalformedJwtException;

@RestControllerAdvice
public class RegistrationControllerAdvice extends ResponseEntityExceptionHandler {

    @ExceptionHandler(SQLException.class)
    public ResponseEntity<GenericResponse> handleSQLException(SQLException e) {
        GenericResponse response = new GenericResponse();
        response.setMessage(e.getMessage());
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(JwtException.class)
    public ResponseEntity<GenericResponse> handleJwtException(JwtException e) {
        GenericResponse response = new GenericResponse();
        response.setMessage(e.getMessage());
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(InternalServerException.class)
    public ResponseEntity<GenericResponse> handleInternalServerException(InternalServerException e) {
        GenericResponse response = new GenericResponse();
        response.setMessage(e.getMessage());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ExceptionHandler(GenericException.class)
    public ResponseEntity<GenericResponse> handleGenericException(GenericException e) {
        GenericResponse response = new GenericResponse();
        response.setMessage(e.getMessage());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ExceptionHandler(MalformedJwtException.class)
    public ResponseEntity<GenericResponse> handleMalformedJwtException(MalformedJwtException e) {
        GenericResponse response = new GenericResponse();
        response.setMessage(e.getMessage());
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(MissingRequestHeaderException.class)
    public ResponseEntity<GenericResponse> handleMissingRequestHeaderException(MissingRequestHeaderException e) {
        GenericResponse response = new GenericResponse();
        response.setMessage(e.getMessage());
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }
}