package com.vfd.mfb.savi.user.exception;

public class GenericException extends Exception {
    public GenericException(String message) { super(message); }
}