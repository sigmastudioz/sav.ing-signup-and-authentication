package com.vfd.mfb.savi.user.models;

import org.hibernate.annotations.NaturalId;
import org.hibernate.annotations.Type;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
public class LoginModel {
    @NotBlank
    private String email;

    @NotBlank
    private String password;
}