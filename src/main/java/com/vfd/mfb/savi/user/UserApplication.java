package com.vfd.mfb.savi.user;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
// import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


// @EnableEurekaClient
@SpringBootApplication
@EnableJpaAuditing
@Configuration
@EnableSwagger2
public class UserApplication {

	@Value("${VERSION:0.0.1}")
	String version;

	public static void main(String[] args) {
		SpringApplication.run(UserApplication.class, args);
	}

	// @Bean
    // public Docket api() { 
    //     return new Docket(DocumentationType.SWAGGER_2)  
    //       .select()                                  
    //       .apis(RequestHandlerSelectors.basePackage("com.vfd.mfb.savi.user"))              
    //       .paths(PathSelectors.any())                          
	// 	  .build()
	// 	  .apiInfo(new ApiInfoBuilder().version(version).title("Sav.ing API").description("Documentation Department API v" + version).build());                                           
	// }
}