package com.vfd.mfb.savi.user.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;
import java.util.Date;

@JsonIgnoreProperties(
    value = {"gender"},
    allowSetters = true
)
@Data
@NoArgsConstructor
public class UserRequestModel extends UserModel {
    private String firstName;
    private String lastName;
    private Long phoneNumber;
    private Long BVN;
    private Date dob;
    private String gender;
    private String email;
    private String changeEmail;
    private String password;
}

