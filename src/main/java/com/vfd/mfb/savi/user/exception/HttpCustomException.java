package com.vfd.mfb.savi.user.exception;

import org.springframework.http.HttpStatus;
import lombok.Data;

@Data
public class HttpCustomException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    private final String message;
    private final HttpStatus httpStatus;

    public HttpCustomException(String message, HttpStatus httpStatus) {
        this.message = message;
        this.httpStatus = httpStatus;
    }
}