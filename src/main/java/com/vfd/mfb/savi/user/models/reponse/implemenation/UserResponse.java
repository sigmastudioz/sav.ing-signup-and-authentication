package com.vfd.mfb.savi.user.models.reponse.implemenation;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.vfd.mfb.savi.user.models.UserModel;
import com.vfd.mfb.savi.user.models.reponse.CommonResponse;

import lombok.Data;

@Data
public class UserResponse implements CommonResponse {
    @JsonProperty("Message")
    private String message;

    @JsonProperty("ReturnCode")
    private int returnCode;

    @JsonProperty
    private UserModel user;
}