package com.vfd.mfb.savi.user.utils.jwt;

public abstract class JWTContants {
    public static final String AUTHORIZATION = "Authorization";
    public static final String BEARER = "Bearer ";
    public static final String PUBLIC_KEY_BEGIN = "-----BEGIN PUBLIC KEY-----";
    public static final String PUBLIC_KEY_END = "-----END PUBLIC KEY-----";
    public static final String PUBLIC_KEY_TYPE = "RSA";
}