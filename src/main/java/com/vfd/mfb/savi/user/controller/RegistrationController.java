package com.vfd.mfb.savi.user.controller;

import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.naming.AuthenticationException;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.vfd.mfb.savi.user.models.UserModel;
import com.vfd.mfb.savi.user.models.reponse.implemenation.GenericResponse;
import com.vfd.mfb.savi.user.exception.GenericException;
import com.vfd.mfb.savi.user.exception.SQLErrorException;
import com.vfd.mfb.savi.user.models.KYCModel;
import com.vfd.mfb.savi.user.models.LoginModel;
import com.vfd.mfb.savi.user.models.LoginResponseModel;
import com.vfd.mfb.savi.user.repository.KYCRepository;
import com.vfd.mfb.savi.user.repository.UserRepository;
// import com.vfd.mfb.savi.user.services.VFDService;
import com.vfd.mfb.savi.user.utils.EmailHelper;
import com.vfd.mfb.savi.user.utils.ShortHash;
import com.vfd.mfb.savi.user.utils.jwt.JWTUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.json.JSONObject;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@CrossOrigin
@RestController
@RequestMapping("/signup/register")
@Slf4j
public class RegistrationController {

    private BCrypt bcrypt;

    private UserModel userModel;

    @Autowired
    private JWTUtils jwtUtils;

    @Autowired
    private EmailHelper emailHelper;

    @Autowired
    private ShortHash shortHash;

    // @Autowired
    // private VFDService vfdService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private KYCRepository kycRepository;

    @Value("${savi.config.siteUrl}")
    private String siteUrl;

    @PostMapping(value="/sign-up", produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserModel> signup(@Valid @RequestBody UserModel user)
    throws SQLErrorException, GenericException {
        log.info("Registering user account with information: {}", user);
        try {
            String userEmail = user.getEmail().toLowerCase();
            userModel = userRepository.findByEmail(userEmail);
            if (userModel == null) {
                log.info("User with e-mail not Found: " + userEmail);
                bcrypt = new BCrypt();
                user.setPassword(bcrypt.hashpw(user.getPassword(), bcrypt.gensalt(12)));
                user.setUserId(shortHash.generateHash());
                user.setEmail(userEmail);

                userModel = userRepository.save(user);


                KYCModel kycModel = new KYCModel();
                kycModel.setUserId(user.getUserId());
                kycRepository.save(kycModel);

                log.info("User created ->>>" + user.getUserId());
                // SEND EMAIL
                Map emailData = new HashMap();
                emailData.put("name", user.getFirstName());
                emailData.put("signature", "Savi.ng");
                emailHelper.sendEmail("Welcome to Savi.ng", user.getEmail(), "user-signup", emailData);

                // CREATE VFD ACCOUNT
                JSONObject data = new JSONObject();
                data.put("firstname", user.getFirstName());
                data.put("lastname", user.getLastName());
                data.put("dateOfBirth", user.getDob());
                data.put("gender", user.getGender().toUpperCase());
                //vfdService.creditUserAccount(data);
                return new ResponseEntity<>(userModel, HttpStatus.OK);
            } else {
                throw new GenericException("User with e-mail already Exists: " + userEmail);
            }
        } catch (Exception e) {
            throw new GenericException(e.getMessage());
        }        
    }

    @PostMapping(value="/password-reset", produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericResponse> resetUserPassword(@RequestBody LoginModel user)
    throws SQLErrorException, GenericException {
        try {
            String userEmail = user.getEmail().toLowerCase();
            userModel = userRepository.findByEmail(userEmail);
            log.info("Reset user password, sending confirmation token", userEmail);
            if (userModel == null) {
                throw new GenericException("User with e-mail does not exists: " + userEmail);
            } else {
                log.info("User with e-mail found: " + userEmail);
                GenericResponse response = new GenericResponse();
                long nowMillis = System.currentTimeMillis();
                Date now = new Date(nowMillis);
                Date passwordExpires = userModel.getResetPasswordExpires();

                if (passwordExpires != null && passwordExpires.getTime() < now.getTime()) {
                    response.setMessage("Your password reset has expired.");
                    return new ResponseEntity<>(response, HttpStatus.OK);
                } else if (passwordExpires != null && passwordExpires.getTime() > now.getTime()) {
                    response.setMessage("You have already initiated a password reset. Please check your email again!");
                } else {
                    long expMillis = nowMillis + 3600000; //Expire in an hr;
                    Date exp = new Date(expMillis);
                    String token = shortHash.getSaltString();

                    userModel.setResetEmailToken(token);
                    userModel.setResetPasswordExpires(exp);

                    userModel = userRepository.save(userModel);
                    response.setMessage("You have initiated a password reset. Please check your email for a reset link!");

                    //PASSWORD FORGOT RESET EMAIL
                    Map emailData = new HashMap();
                    emailData.put("name", userModel.getFirstName());
                    emailData.put("resetUrl", siteUrl+"/verify/email/"+token);
                    emailData.put("signature", "Savi.ng");
                    emailHelper.sendEmail("Savi.ng Password Reset", userModel.getEmail(), "password-reset", emailData);
                }
                
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            throw new GenericException(e.getMessage());
        }        
    }

    @PostMapping(value="/verify/email/{token}", produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericResponse> verifyToken(@PathVariable String token, @RequestBody UserModel user)
    throws SQLErrorException, GenericException {
        log.info("Verifying user account with token: {}", token);
        try {
            userModel = userRepository.findByResetEmailToken(token);
            GenericResponse response = new GenericResponse();

            if (userModel == null) {
                throw new GenericException("Reset token does not exists!");
            } else {
                if (user.getPassword() != null) {
                    bcrypt = new BCrypt();
                    userModel.setResetEmailToken(null);
                    userModel.setResetPasswordExpires(null);
                    // @TODO CHECK FOR PREVIOUS PASSWORDS USED
                    userModel.setPassword(bcrypt.hashpw(user.getPassword(), bcrypt.gensalt(12)));
                    userModel = userRepository.save(userModel);
                    // @TODO SEND PASSWORD UPDATE EMAIL
                    response.setMessage("You have updated your password!");
                } else {
                    throw new GenericException("Password required!");
                }
            }

            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            throw new GenericException(e.getMessage());
        }        
    }

    @PostMapping(value="/sms/verify", produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserModel> verifySMSCode(@RequestBody String phoneNumber)
    throws SQLErrorException, GenericException {
        log.info("Verifying user account with token: {}", phoneNumber);
        try {
            throw new GenericException("Generic Placeholder");
        } catch (Exception e) {
            throw new GenericException(e.getMessage());
        }        
    }

    @PostMapping(value="/sms/send", produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserModel> sendSMSCode(@RequestBody String phoneNumber)
    throws SQLErrorException, GenericException {
        log.info("Sending confirmation digits user account with token: {}", phoneNumber);
        try {
            throw new GenericException("Generic Placeholder");
        } catch (Exception e) {
            throw new GenericException(e.getMessage());
        }        
    }

    @PostMapping(value="/login", produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<LoginResponseModel> login(@Valid @RequestBody LoginModel login, ServletRequest request)
    throws SQLException, GenericException, AuthenticationException {
        LoginResponseModel responseModel = new LoginResponseModel();
        bcrypt = new BCrypt();

        log.info("Logining user with information: {}");
        try {
            String userEmail = login.getEmail().toLowerCase();
            userModel = userRepository.findByEmail(userEmail);
            if (userModel != null) {
                if (bcrypt.checkpw(login.getPassword(), userModel.getPassword())) {   
                    HttpServletRequest httpRequest = (HttpServletRequest) request;                
                    String userId = userModel.getUserId();
                    UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
                        userModel.getEmail(),
                        userModel.getPassword()
                    );
       
                    authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpRequest));
                    SecurityContextHolder.getContext().setAuthentication(authentication);

                    //Sign JWT Token
                    int expiration = 3600000; //Expire in an hr
                    String token = jwtUtils.createJWT(userModel, expiration);
                    responseModel.setUserId(userId);
                    responseModel.setToken(token);
                    return new ResponseEntity<>(responseModel, HttpStatus.OK);
                } else {
                     //Check for sign attempts
                    throw new GenericException("Password does not match!");
                }
            } else {
                throw new SQLErrorException("User with e-mail does not exists: " + userEmail);
            }            
        } catch (Exception e) {
            throw new GenericException(e.getMessage());
        }
    }
}