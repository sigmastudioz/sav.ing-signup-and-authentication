package com.vfd.mfb.savi.user.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorldController {

    @GetMapping("/signup/")
    public String hello(){ return "Testing Deployment from BitBucket"; }

}
