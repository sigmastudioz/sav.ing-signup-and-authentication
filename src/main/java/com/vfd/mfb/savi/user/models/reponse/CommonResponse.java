package com.vfd.mfb.savi.user.models.reponse;

public interface CommonResponse {
    String getMessage();
    void setMessage(String message);

    int getReturnCode();
    void setReturnCode(int returnCode);
}