package com.vfd.mfb.savi.user.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.vfd.mfb.savi.user.exception.GenericException;
import com.vfd.mfb.savi.user.models.KYCModel;
import com.vfd.mfb.savi.user.models.UserModel;
import com.vfd.mfb.savi.user.repository.KYCRepository;
import com.vfd.mfb.savi.user.repository.UserRepository;
import com.vfd.mfb.savi.user.services.UploadService;
import com.vfd.mfb.savi.user.utils.jwt.JWTUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.NonNull;

@RestController
@RequestMapping("/upload")
public class UploadController {

    @Autowired
    private UploadService uploadService;

    @Autowired
    private KYCRepository kycRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private JWTUtils jwtUtils;

    /**
     * Extracts the file payload from an HttpServletRequest, checks that the file
     * extension is supported and uploads the file to Google Cloud Storage.
     */
    @PostMapping(value = "/user/{type}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<KYCModel> getImageUrl(
        @RequestHeader(value="Authorization") String token,
        HttpServletRequest req, 
        HttpServletResponse resp,
        @NonNull @PathVariable String type
    )
        throws IOException, ServletException, GenericException {
            UserModel user = jwtUtils.decodeJWT(token);
            String userId = user.getUserId();
            UserModel userModel = userRepository.findByUserId(userId);
            if (userModel != null && userModel.getUserId().contains(userId)) {
                Part filePart = req.getPart("file");
                final String fileName = filePart.getSubmittedFileName();
                String imageUrl = req.getParameter("imageUrl");
                // Check extension of file
                if (fileName != null && !fileName.isEmpty() && fileName.contains(".")) {
                    final String extension = fileName.substring(fileName.lastIndexOf('.') + 1);
                    String[] allowedExt = { "jpg", "jpeg", "png", "gif" };
                    for (String s : allowedExt) {
                        if (extension.equals(s)) {
                            String url = null;
                            KYCModel kycModel = kycRepository.findByUserId(userId);
                            if (kycModel == null) {
                                kycModel = new KYCModel();
                                kycModel.setUserId(userId);
                            }

                            if (type.toLowerCase().equals("profile") && !kycModel.isVerifiedProfileImage()) {
                                url = uploadService.uploadFile("users/"+userId+"/profile/", filePart, userId);
                                kycModel.setProfileImage(url);
                            }

                            if (type.toLowerCase().equals("identification") && !kycModel.isVerifiedID()) {
                                url = uploadService.uploadFile("users/"+userId+"/identification/", filePart, userId);
                                kycModel.setIdImage(url);
                            }

                            kycModel = kycRepository.save(kycModel);
                            return new ResponseEntity<>(kycModel, HttpStatus.OK);
                        }
                    }
                    throw new ServletException("file must be an image");
                } else {
                    throw new GenericException(imageUrl);
                }
            } else {
                throw new GenericException("User not found!");
            }
    }
}