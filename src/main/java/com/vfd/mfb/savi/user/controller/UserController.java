package com.vfd.mfb.savi.user.controller;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.validation.Valid;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.vfd.mfb.savi.user.exception.GenericException;
import com.vfd.mfb.savi.user.models.KYCModel;
import com.vfd.mfb.savi.user.models.UserModel;
import com.vfd.mfb.savi.user.models.UserRequestModel;
import com.vfd.mfb.savi.user.repository.KYCRepository;
import com.vfd.mfb.savi.user.repository.UserRepository;
import com.vfd.mfb.savi.user.utils.EmailHelper;
import com.vfd.mfb.savi.user.utils.jwt.JWTUtils;

import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.jsonwebtoken.JwtException;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/signup/users")
@Slf4j
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private KYCRepository kycRepository;

    @Autowired
    private JWTUtils jwtUtils;

    @Autowired
    private EmailHelper emailHelper;

    private BCrypt bcrypt;

    private Type lisType;

    private LoadingCache<String, String> cache;

    private Gson gson;

    public UserController () {
        //in memory cache. Can substitute with a distributed cache later.
        this.cache = CacheBuilder.newBuilder()
        .expireAfterAccess(45, TimeUnit.MINUTES)
        .build(
            new CacheLoader<String, String>() {
                @Override
                public String load(String key) {
                    return key.toUpperCase();
                }
            });

        this.lisType = new TypeToken<UserModel>() {}.getType();
        this.gson = new Gson();
    }

    @PostMapping(value="/user", produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserModel> getUser(@RequestHeader(value="Authorization") String token) throws GenericException {
        try {
            log.info(jwtUtils.getJWTToken(token));
            UserModel user = jwtUtils.decodeJWT(token);
            String userId = user.getUserId();
            UserModel userModel = null;
            String cacheData = cache.getUnchecked(userId);

            if (cacheData.equals(userId.toUpperCase()) || cacheData == null) {
                userModel = userRepository.findByUserId(userId);
                cache.put(userId, gson.toJson(userModel));
            } else {
                userModel = new Gson().fromJson(cacheData, lisType);
            }
            return new ResponseEntity<>(userModel, HttpStatus.OK);
        } catch (Exception e) {
            throw new GenericException(e.getMessage());
        }
    }

    @PutMapping(value="/user/{userId}", produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserModel> getUser(
        @RequestHeader(value="Authorization") String token, 
        @RequestBody UserRequestModel userRequest
    ) throws GenericException {
        try {
            UserModel user = jwtUtils.decodeJWT(token);
            String userId = user.getUserId();
            UserModel userModel = userRepository.findByUserId(userId);
            if (userModel != null && userModel.getUserId().contains(userId)) {
                String password = userRequest.getPassword();
                String email = userRequest.getEmail();
                String firstName = userRequest.getFirstName();
                String lastName = userRequest.getLastName();
                Long phoneNumber = userRequest.getPhoneNumber();

                Boolean passwordChanged = false;
                if (password != null && !bcrypt.checkpw(password, userModel.getPassword())) {
                    bcrypt = new BCrypt();
                    userRequest.setPassword(bcrypt.hashpw(password, bcrypt.gensalt(12)));
                    passwordChanged = true;
                    userModel.setPassword(userRequest.getPassword());
                }

                if (email != null) {
                    userModel.setEmail(email);
                }
                if (firstName != null) {
                    userModel.setFirstName(firstName);
                }
                if (lastName != null) {
                    userModel.setLastName(lastName);
                }
                if (phoneNumber != null) {
                    userModel.setPhoneNumber(phoneNumber);
                }

                userModel = userRepository.save(userModel);
                cache.put(userId, gson.toJson(userModel));

                // SEND EMAIL UPDATE
                if (userModel != null && passwordChanged) {
                    Map emailData = new HashMap();
                    emailData.put("name", userModel.getFirstName());
                    emailData.put("signature", "Savi.ng");
                    emailHelper.sendEmail("Savi.ng Password Update", userModel.getEmail(), "password-update", emailData);
                }
                return new ResponseEntity<>(userModel, HttpStatus.OK);
            } else {
                throw new GenericException("User not found!");
            }
            
        } catch (Exception e) {
            throw new GenericException(e.getMessage());
        }
    }

    @GetMapping(value="/user/{userId}/kyc", produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<KYCModel> getKYC(
        @RequestHeader(value="Authorization") String token
    ) throws GenericException {
        try {
            UserModel user = jwtUtils.decodeJWT(token);
            String userId = user.getUserId();
            UserModel userModel = userRepository.findByUserId(userId);
            KYCModel kycModel = kycRepository.findByUserId(userId);

            if (userModel != null && userModel.getUserId().contains(userId) && kycModel != null) {
                return new ResponseEntity<>(kycModel, HttpStatus.OK);
            } else {
                throw new GenericException("KYC not found!");
            }
            
        } catch (Exception e) {
            throw new GenericException(e.getMessage());
        }
    }

    @PutMapping(value="/user/{userId}/kyc", produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<KYCModel> updateKYC(
        @RequestHeader(value="Authorization") String token, 
        @RequestBody KYCModel kycRequestModel
    ) throws GenericException {
        try {
            UserModel user = jwtUtils.decodeJWT(token);
            String userId = user.getUserId();
            UserModel userModel = userRepository.findByUserId(userId);

            if (userModel != null && userModel.getUserId().contains(userId)) {
                KYCModel kycModel = kycRepository.findByUserId(userId);
                if (kycModel == null) {
                    kycModel = new KYCModel();
                    kycModel.setUserId(userId);
                }

                if (!kycModel.isVerifiedBVN()) {
                    kycModel.setBVN(kycRequestModel.getBVN());
                }

                if (!kycModel.isVerifiedDepositBank()) {
                    kycModel.setDepositBank(kycRequestModel.getDepositBank());
                    kycModel.setDepositBankCode(kycRequestModel.getDepositBankCode());
                }

                kycModel = kycRepository.save(kycModel);
                return new ResponseEntity<>(kycModel, HttpStatus.OK);
            } else {
                throw new GenericException("User not found!");
            }
            
        } catch (Exception e) {
            throw new GenericException(e.getMessage());
        }
    }
}