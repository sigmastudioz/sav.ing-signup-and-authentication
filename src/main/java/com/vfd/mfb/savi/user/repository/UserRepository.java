package com.vfd.mfb.savi.user.repository;

import com.vfd.mfb.savi.user.exception.SQLErrorException;
import com.vfd.mfb.savi.user.models.UserModel;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<UserModel, Long> {
    // Optional<Customer> findByEmail(String email);

    // List<Customer> findByCustomerIdIn(List<Long> customerId);

    // Boolean existsByEmail(String email);

    // @Query(value = "SELECT * FROM USERS WHERE userId = ?1", nativeQuery = true)
    // UserModel getByUserId(String userId);

    // // @Query(value = "SELECT * FROM USERS WHERE userId = ?1", nativeQuery = true)
    // // List<UserModel> getAllUsers(String userId);

    // @Query(value = "SELECT * FROM USERS WHERE email = ?1", nativeQuery = true)
    UserModel findByEmail(String email);
    UserModel findByResetEmailToken(String resetEmailToken);
    UserModel findByUserId(String userId);
}