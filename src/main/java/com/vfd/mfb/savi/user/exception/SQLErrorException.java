package com.vfd.mfb.savi.user.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class SQLErrorException extends Exception {
    public SQLErrorException(String message) { super(message); }
}