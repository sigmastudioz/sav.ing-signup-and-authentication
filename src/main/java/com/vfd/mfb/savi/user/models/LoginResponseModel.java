package com.vfd.mfb.savi.user.models;

import lombok.Data;

@Data
public class LoginResponseModel {
    private String token;
    private String userId;
}