package com.vfd.mfb.savi.user.repository;

import com.vfd.mfb.savi.user.exception.SQLErrorException;
import com.vfd.mfb.savi.user.models.KYCModel;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface KYCRepository extends JpaRepository<KYCModel, Long> {
    KYCModel findByUserId(String userId);
}