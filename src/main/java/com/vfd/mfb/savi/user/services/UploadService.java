package com.vfd.mfb.savi.user.services;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.TimeZone;

import javax.servlet.http.Part;

import com.google.cloud.storage.Acl;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import com.google.cloud.storage.Acl.Role;
import com.google.cloud.storage.Acl.User;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import lombok.NonNull;

@Service
public class UploadService {

    @Value("${google.storage.user.bucket}")
    private String bucketName;

    private Storage storage;

    public UploadService() {
        this.storage = StorageOptions.getDefaultInstance().getService();
    }

    /**
     * Uploads a file to Google Cloud Storage to the bucket specified in the
     * BUCKET_NAME environment variable, appending a timestamp to end of the
     * uploaded filename.
     */
    @SuppressWarnings("deprecation")
    public String uploadFile(@NonNull String imagePath, Part filePart, String userId) throws IOException {
        DateFormat df = new SimpleDateFormat("-YYYY-MM-dd-HHmmssSSS");
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        
        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);
        String dtString = df.format(now);
        final String fileName = imagePath+filePart.getSubmittedFileName();

        // the inputstream is closed by default, so we don't need to close it here
        BlobInfo blobInfo = storage.create(BlobInfo.newBuilder(bucketName, fileName)
                // Modify access list to allow all users with link to read file
                .setAcl(new ArrayList<>(Arrays.asList(Acl.of(User.ofAllUsers(), Role.READER)))).build(),
                filePart.getInputStream());
        // return the public download link
        return blobInfo.getMediaLink().replace("%2F", "/");
    }
}