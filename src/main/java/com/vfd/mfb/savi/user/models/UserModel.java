package com.vfd.mfb.savi.user.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.vfd.mfb.savi.user.utils.ShortHash;

import org.hibernate.annotations.NaturalId;
import org.hibernate.annotations.Type;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;
import java.util.Date;

@JsonIgnoreProperties(
    value = {"id", "gender", "password", "createdAt", "updatedAt", "changeEmail", "resetPasswordToken", "resetPasswordExpires", "resetEmailToken", "verifiedNumber", "userVerified"},
    allowSetters = true
)
@Data
@NoArgsConstructor
@Entity
@Table(name = "customers", uniqueConstraints = {
    @UniqueConstraint(columnNames = {
        "email"
    }),
    @UniqueConstraint(columnNames = {
        "phoneNumber"
    })
})
public class UserModel extends AuditModel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long Id;

    @NotBlank
    @Column(columnDefinition = "VARCHAR(50)", nullable = false)
    private String firstName;

    @NotBlank
    @Column(columnDefinition = "VARCHAR(50)", nullable = false)
    private String lastName;

    @NotNull
    
    @Column(columnDefinition = "VARCHAR(15)", nullable = false, unique=true)
    private Long phoneNumber;

    @NotNull
    @Column(columnDefinition = "BOOLEAN default false")
    private boolean verifiedNumber;

    @NotNull
    @Column(columnDefinition = "BOOLEAN default false")
    private boolean userVerified;

    @NotNull
    @Column(columnDefinition = "DATE default null")
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date dob;

    @NotBlank
    @Column(columnDefinition = "VARCHAR(1)", nullable = false)
    private String gender;

    @Column(columnDefinition = "VARCHAR(50)", nullable = false)
    private String userId;

    @NotBlank
    @NaturalId
    @Size(max = 320)
    @Column(columnDefinition = "VARCHAR(320)", nullable = false)
    private String email;

    @Size(max = 320)
    @Column(columnDefinition = "VARCHAR(320) default null")
    private String changeEmail;

    @NotBlank
    @Column(columnDefinition = "VARCHAR(100)", nullable = false)
    private String password;

    @Column(columnDefinition = "VARCHAR(100) default null")
    private String resetPasswordToken;
    
    @Column(columnDefinition = "TIMESTAMP")
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date resetPasswordExpires;

    @Column(columnDefinition = "VARCHAR(100) default null")
    private String resetEmailToken;
}

