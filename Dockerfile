FROM openjdk:8-jdk-alpine
VOLUME /tmp
COPY target/savi-user-0.0.1-SNAPSHOT.jar savi-user-0.0.1-SNAPSHOT.jar
CMD ["java","-jar","/savi-user-0.0.1-SNAPSHOT.jar"]